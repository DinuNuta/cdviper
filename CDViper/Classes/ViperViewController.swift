//
//  ViperViewController.swift
//  CDViper
//
//  Created by Coscodan Dinu on 11/16/18.
//

import Foundation


open class ViperViewController : UIViewController, ViperModuleTransitionHandler {
    
    open var moduleInput: ViperModuleInput? {
        return nil
    }
    
    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handle(segue: segue, wrapper: sender)
    }
}
