//
//  ViperModuleTransitionHandler.swift
//  CDViper
//
//  Created by Coscodan Dinu on 11/16/18.
//

import Foundation

public enum ViperPresentationType {
    case modal(animated:Bool,completion:(()->())?)
    case push(animated:Bool)
}

public typealias ConfigurationBlock = (ViperModuleInput) -> ()

public class SegueConfigBlockWrapper : NSObject {
    public var configurationBlock: ConfigurationBlock = {_ in}
    public var configurator: ViperModuleConfigurator? = nil
    public var transitioningDelegate: UIViewControllerTransitioningDelegate? = nil
}

public protocol ViperModuleTransitionHandler : class {
    
    var moduleInput : ViperModuleInput? {get}
    func openModule(defaultSegueIdentifierFor:UIViewController.Type,
                    configurator: ViperModuleConfigurator?,
                    customTransitioningDelegate:UIViewControllerTransitioningDelegate?,
                    configurationBlock: @escaping ConfigurationBlock)
    
    func openModule(customSegueIdentifier:String,
                    configurator: ViperModuleConfigurator?,
                    customTransitioningDelegate:UIViewControllerTransitioningDelegate?,
                    configurationBlock: @escaping ConfigurationBlock)
    
    func openModule<T:UIViewController>(viewController:T.Type,
                                        presentationType:ViperPresentationType,
                                        configurator: ViperModuleConfigurator?,
                                        customTransitioningDelegate:UIViewControllerTransitioningDelegate?,
                                        configurationBlock: @escaping ConfigurationBlock)
    
    func closeModule(animated:Bool, completion:(()->())?)
}

public extension ViperModuleTransitionHandler where Self : UIViewController {
    
    public func openModule(defaultSegueIdentifierFor:UIViewController.Type,
                           configurator: ViperModuleConfigurator?,
                           customTransitioningDelegate:UIViewControllerTransitioningDelegate?,
                           configurationBlock: @escaping ConfigurationBlock) {
        let wrapper = SegueConfigBlockWrapper()
        wrapper.configurationBlock = configurationBlock
        wrapper.configurator = configurator
        wrapper.transitioningDelegate = customTransitioningDelegate
        performSegue(withIdentifier: defaultSegueIdentifierFor.defaultStoryboardName, sender: wrapper)
    }
    
    public func openModule(customSegueIdentifier:String,
                           configurator: ViperModuleConfigurator?,
                           customTransitioningDelegate:UIViewControllerTransitioningDelegate?,
                           configurationBlock: @escaping ConfigurationBlock) {
        let wrapper = SegueConfigBlockWrapper()
        wrapper.configurationBlock = configurationBlock
        wrapper.configurator = configurator
        wrapper.transitioningDelegate = customTransitioningDelegate
        performSegue(withIdentifier: customSegueIdentifier, sender: wrapper)
    }
    
    public func openModule<T:UIViewController>(viewController:T.Type,
                                               presentationType:ViperPresentationType,
                                               configurator: ViperModuleConfigurator?,
                                               customTransitioningDelegate:UIViewControllerTransitioningDelegate?,
                                               configurationBlock: @escaping ConfigurationBlock) {
        let viewController = T()
        
        configurator?.configure(with: viewController)
        transitioningDelegate.flatMap{ viewController.transitioningDelegate = $0 }
        
        if let transitionHandler = viewController as? ViperModuleTransitionHandler {
            transitionHandler.moduleInput.flatMap{configurationBlock($0)}
        }
        
        switch presentationType {
        case .modal(animated: let animated, completion: let completion):
            present(viewController, animated: animated, completion: completion)
        case .push(animated: let animated):
            navigationController?.pushViewController(viewController, animated: animated)
        }
    }
    
    public func closeModule(animated:Bool, completion:(()->())?) {
        if let _ = navigationController {
            navigationController?.popViewController(animated: true)
        }
        
        if let _ = presentingViewController {
            dismiss(animated: true, completion: completion)
        }
    }
}

extension ViperModuleTransitionHandler where Self:UIViewController {
    func handle(segue:UIStoryboardSegue, wrapper:Any?) {
        guard let transitionHandler = segue.destination.bypassNavigationController() as? ViperModuleTransitionHandler else {return}
        guard let wrapper = wrapper as? SegueConfigBlockWrapper else {return}
        
        if let vc = (transitionHandler as? UIViewController) {
            wrapper.configurator?.configure(with: vc)
            wrapper.transitioningDelegate.flatMap{ vc.transitioningDelegate = $0 }
        }
        
        transitionHandler.moduleInput.flatMap{ wrapper.configurationBlock($0) }
    }
    
    func configure(with configurator:ViperModuleConfigurator?, transitioningDelegate:UIViewControllerTransitioningDelegate?, configurationBlock:@escaping ConfigurationBlock) {
        configurator?.configure(with: self)
        transitioningDelegate.flatMap{ self.transitioningDelegate = $0 }
        moduleInput.flatMap{ configurationBlock($0) }
    }
}

extension UIViewController {
    static var defaultStoryboardName: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    func bypassNavigationController() -> UIViewController? {
        if let navigationController = self as? UINavigationController {
            return navigationController.viewControllers.first
        }else {
            return self
        }
    }
}
